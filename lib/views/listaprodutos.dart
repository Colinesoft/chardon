import 'package:chardon/globais.dart';
import 'package:chardon/models/items.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:url_launcher/url_launcher.dart';

//PAGINA
class Imagem extends StatelessWidget {
  Imagem(this.imagem, this.descricao);
  String imagem;
  String descricao;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.descricao),
      ),
      body: Center(
        child: Image.asset(this.imagem),
      ),
    );
  }
}
// FIM PAGINA
//PAGINA PDF
class PDF extends StatefulWidget {
  PDF(this._pdfdoc);
  String _pdfdoc;

  @override
  _PDFState createState() => _PDFState();
}
class _PDFState extends State<PDF> {
  bool _isLoading=false, _isInit=false;

  PDFDocument document;
    @override
  void initState() {
    loadFromAssets();
    super.initState();
  }  
  

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Column(
          children: <Widget>[
            Expanded(
              child: Center(
                child: _isInit 
                  ? Text('Press to Load...')
                  : _isLoading
                    ? Center(
                      child: CircularProgressIndicator(),
                    )
                    : PDFViewer(
                      document: document,
                    )
              ),
            )
          ],
        ),
      ),
    );
  }

  loadFromAssets() async {
    setState((){
      _isInit = false;
      _isLoading = true;
    });
    document = await PDFDocument.fromURL(widget._pdfdoc);
    setState(() {
      _isLoading = false;
    });
    
  }
}
// FIM PAGINA


class ListaProdutos extends StatefulWidget {
  @override
  _ListaProdutosState createState() => _ListaProdutosState();
}

class _ListaProdutosState extends State<ListaProdutos> {
  List<ItemChardon> _notes = List<ItemChardon>();
  List<ItemChardon> _notesForDisplay = List<ItemChardon>();

  var itemsChardom = new List<ItemChardon>();

  Future<List<ItemChardon>> fetchNotes() async {
    var notes = ListaProdutos();    
    return notes;
  }

  @override
  void initState() {
    fetchNotes().then((value) {
      setState(() {
        _notes.addAll(value);
        _notesForDisplay = _notes;
      });
    });
    super.initState();
  }  

  ListaProdutos() {
    int posicao = 0;
    int pagina = 1 ;
    String categoria = "IEEE/ANSI 200A Loadbreak";
    itemsChardom = [];
    //1
    posicao++;
    itemsChardom.add(ItemChardon( 
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria" ,
      descricao       : "15 kV 200A Loadbreak Elbow TDC",
      partnumber      : "15-LE200",
      campopesquisa   : "15-LE200 165LR-C-5250 LE215 215LE ELB-15-210 IEEE/ANSI 200A Loadbreak 15 kV 200A Loadbreak Elbow TDC",
      assetImagem     : "assets/products/P001/001.jpg",
      assetImagemMini : "assets/products/P001/mini/001.png",
      //catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LE200.pdf",
      //instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25LE200-_Instruction_Sheet-20171128.pdf",     
      catalogo        : "http://189.79.77.141/chardon/catalogs/P001/001.pdf",
      instrucao       : "http://189.79.77.141/chardon/instructions/P001/001.pdf",     
      video           : "https://www.youtube.com/watch?v=xC5ysQid1Kc&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn&index=5", 
    ));
    //2
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Bushing Insert PIS",
      partnumber      : "15-LBI200",
      campopesquisa   : "15-LBI200 1601A4 LBI215 215BI ELB-15-200-BI IEEE/ANSI 200A Loadbreak 15 kV 200A Bushing Insert PIS",
      assetImagem     : "assets/products/P001/002.jpg",
      assetImagemMini : "assets/products/P001/mini/002.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LBI200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LBI200-INSTRUCTION_SHEET-20170719.pdf",
      video           : "", 
    ));
    //3
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Loadbreak Protective Cap RIB",
      partnumber      : "15-LIC200",
      campopesquisa   : "15-LIC200 160DR LPC215 215CI ELB-15-200-IC IEEE/ANSI 200A Loadbreak 15 kV 200A Loadbreak Protective Cap RIB",
      assetImagem     : "assets/products/P001/003.jpg",
      assetImagemMini : "assets/products/P001/mini/003.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LIC200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LBI200-INSTRUCTION_SHEET-20170719.pdf",
      video           : "", 
    ));
    //4
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Grounding Elbow TDC-AT",
      partnumber      : "15-GLE200",
      campopesquisa   : "15-GLE200 160DRG GE215 T6003091 $categoria 15 kV 200A Grounding Elbow TDC-AT",
      assetImagem     : "assets/products/P001/00$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15GLE.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7610540_15-25kV_GROUNDING_ELBOW_Instructions.pdf",
      video           : "", 
    )); 
    //5
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Loadbreak Junction BDX/BQX/BTX",
      partnumber      : "15-LJ200",
      campopesquisa   : "$categoria 15 kV 200A Loadbreak Junction BDX/BQX/BTX 15-LJ200 ",
      assetImagem     : "assets/products/P001/00$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15LJ200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A0721042-1_15_25kV_200A_JUNCTION_Instructions.pdf",
      video           : "", 
    )); 
    //6
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Feed-Thru Insert PID",
      partnumber      : "15-LFTI",
      campopesquisa   : "$categoria 15 kV 200A Feed-Thru Insert PID 15-LFTI200",
      assetImagem     : "assets/products/P001/00$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LFTI.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7910040_15-25_kV_200A_Rotatable_Feed-Thru_Insert_Instructions.pdf",
      video           : "", 
    )); 
    //7
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Loadbreak Fuse Elbow TDC-F",
      partnumber      : "15-LFE200",
      campopesquisa   : "$categoria 15 kV 200A Loadbreak Fuse Elbow TDC-F 15-LFE200 168FLR1 LFEP215TFEC 215FE",
      assetImagem     : "assets/products/P001/00$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LJ200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25kV_200A_Fuse_Elbow_Instruction_Sheet.pdf",
      video           : "https://www.youtube.com/watch?v=X-3_ksuNL_o&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
    )); 
    //8
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Loadbreak Portable Feed Thru PBP",
      partnumber      : "15-LPFT200",
      campopesquisa   : "$categoria 15 kV 200A Loadbreak Portable Feed Thru PBP 15-LPFT200",
      assetImagem     : "assets/products/P001/00$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LJ200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LFT200-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    )); 
    //9
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Insulated Standoff Bushing",
      partnumber      : "15-SOB200",
      campopesquisa   : "$categoria 15 kV 200A Insulated Standoff Bushing 15-SOB200",
      assetImagem     : "assets/products/P001/00$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/15SOB200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //10
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Loadbreak Elbow TDC",
      partnumber      : "25-LE200",
      campopesquisa   : "$categoria 25 kV 200A Loadbreak Elbow TDC 25-LE200",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LE200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25LE200-_Instruction_Sheet-20171128.pdf",
      video           : "https://www.youtube.com/watch?v=xC5ysQid1Kc&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
    )); 
    //11
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Bushing Insert PIS",
      partnumber      : "25-LBI200",
      campopesquisa   : "$categoria 25 kV 200A Bushing Insert PIS 25-LBI200 2701A4 LBI225 225BI ELB-25-200-BI",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LBI200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LBI200-INSTRUCTION_SHEET-20170719.pdf",
      video           : "", 
    )); 
    //12
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Loadbreak Protective Cap RIB",
      partnumber      : "25-LIC200",
      campopesquisa   : "$categoria 25 kV 200A Loadbreak Protective Cap RIB 25-LIC200 ELB-25-200-IC",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LIC200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A1020042-1-15-25kV_200A_INSULATING_CAP_Instructions.pdf",
      video           : "", 
    )); 
    //13
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A  Grounding Elbow",
      partnumber      : "25-GLE200",
      campopesquisa   : "$categoria 25 kV 200A  Grounding Elbow 25-GLE200 273DRG LPC225 9U01BEW500",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15GLE.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7610540_15-25kV_GROUNDING_ELBOW_Instructions.pdf",
      video           : "", 
    )); 
    //14
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Loadbreak Junction 3 or 5 Way (SSBracket)",
      partnumber      : "25-LJ200",
      campopesquisa   : "$categoria 25 kV 200A Loadbreak Junction 3 or 5 Way (SSBracket) 25-LJ200 25-LJ200F3 25-LJ200F4 274J3 274J4 LJ215C4B 228J3B 228J4B",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/25LJ200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A0721042-1_15_25kV_200A_JUNCTION_Instructions.pdf",
      video           : "", 
    )); 
    //15
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Feed-Thru Insert",
      partnumber      : "25-LFTI",
      campopesquisa   : "$categoria 25 kV 200A Feed-Thru Insert 25-LFTI",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25LFT1200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/A7910040_15-25_kV_200A_Rotatable_Feed-Thru_Insert_Instructions.pdf",
      video           : "", 
    )); 
    //16
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV Loadbreak Fuse Elbow",
      partnumber      : "25-LFE200",
      campopesquisa   : "$categoria 25 kV Loadbreak Fuse Elbow 25-LFE200 274FLR1 LFEP228TFEC 228FE",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/25LFE200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/15-25kV_200A_Fuse_Elbow_Instruction_Sheet.pdf",
      video           : "https://www.youtube.com/watch?v=X-3_ksuNL_o&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
    )); 
    //17
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Loadbreak Portable Feed Thru	",
      partnumber      : "25-LPFT200",
      campopesquisa   : "$categoria 25 kV 200A Loadbreak Portable Feed Thru	25-LPFT200",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/15LPFT.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LFT200-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    )); 
    //18
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV 200A Insulated Stanfoff Bushing",
      partnumber      : "25-SOB200",
      campopesquisa   : "$categoria 25 kV 200A Insulated Stanfoff Bushing 25-SOB200",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/25SOB200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //19
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Shield Adapter",
      partnumber      : "SADP",
      campopesquisa   : "$categoria Shield Adapter SADP",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/SADP.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/SADP-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    )); 
    //20
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15kV and 25kV Elbow Arrester",
      partnumber      : "15/25-LEA",
      campopesquisa   : "$categoria 15kV and 25kV Elbow Arrester 15-LEA 25-LEA 15/25-LEA",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/SpanishCatalog/IEEELB200A/1525LEA.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/1525LEA-INSTRUCTION_SHEET-20161123.pdf",
      video           : "", 
    )); 
    //21
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV 200A Loadbreak Elbow-Large Interface",
      partnumber      : "35L-LE200",
      campopesquisa   : "$categoria 35 kV 200A Loadbreak Elbow-Large Interface 35L-LE200 LE235 236LE",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LE200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //22
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV 200A Loadbreak Elbow",
      partnumber      : "35-LE200",
      campopesquisa   : "$categoria 35 kV 200A Loadbreak Elbow 35-LE200 376LR",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LE200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //23
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV 200A Loadbreak Bushing Insert",
      partnumber      : "35-LBI200",
      campopesquisa   : "$categoria 35 kV 200A Loadbreak Bushing Insert 35-LBI200 3701A3",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LBI200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //24
    posicao++;
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV 200A Loadbreak Protective Cap",
      partnumber      : "35-LIC200",
      campopesquisa   : "$categoria 35 kV 200A Loadbreak Protective Cap 35-LIC200",
      assetImagem     : "assets/products/P001/0$posicao.jpg",
      assetImagemMini : "assets/products/P001/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ALOADBREAKCTCONLY/35LIC200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/25DE200-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    )); 
    posicao=0;




    //PÁGINA 2
    //001
    pagina=2;
    posicao++;
    categoria = "IEEE/ANSI 200A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV, 200A Deadbreak Elbow",
      partnumber      : "25-DE200",
      campopesquisa   : "$categoria 25 kV, 200A Deadbreak Elbow 25-DE200",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DE200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/25DS200-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    )); 
    //002
    pagina=2;
    posicao++;
    categoria = "IEEE/ANSI 200A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV, 200A Deadbreak Straight",
      partnumber      : "25-DS200",
      campopesquisa   : "$categoria 25 kV, 200A Deadbreak Straight 25-DS200",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DS200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //003
    pagina=2;
    posicao++;
    categoria = "IEEE/ANSI 200A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV, 200A Deadbreak Bushing Insert",
      partnumber      : "25-DBI200",
      campopesquisa   : "$categoria 25 kV, 200A Deadbreak Bushing Insert 25-DBI200",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DBI200.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/200A/25DBI200-INSTRUCTION_SHEET-20161123.pdf",
      video           : "", 
    )); 
    //004
    pagina=2;
    posicao++;
    categoria = "IEEE/ANSI 200A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25kV 200A Insulated Protective Cap",
      partnumber      : "25-DIC200",
      campopesquisa   : "$categoria 25kV 200A Insulated Protective Cap 25-DIC200",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/200ADEADBREAKCTCONLY/25DIC200.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    posicao = 0;


    //PAGINA 3 001
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15/25 kV, 600A / 900A, T-Body",
      partnumber      : "15/25-TB600",
      campopesquisa   : "$categoria 15/25 kV, 600A / 900A, T-Body 15/25-TB600T 15-TB600T 25-TB600T K655LR-W0X BT625T 625TBT ELB-15/28-600T ELB-15/28-610",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/1525TB600.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/25-TBODY-INSTALLATION_SHEET-170921.pdf",
      video           : "https://www.youtube.com/watch?v=r1upfG1c8OI&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn", 
    )); 
    //PAGINA 3 002
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15/25 kV, 600A Insulated Protective Cap",
      partnumber      : "25-DIC600",
      campopesquisa   : "$categoria 15/25 kV, 600A Insulated Protective Cap 25-DIC600",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25DIC600.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A1040051_15_25_35kV__600A_INSULATING_CAP_Instruction.pdf",
      video           : "", 
    )); 
    //PAGINA 3 003
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15/25 kV, 600A Deadbreak Junction",
      partnumber      : "25-DJ600",
      campopesquisa   : "$categoria 15/25 kV, 600A Deadbreak Junction 25-DJ600F2 K650J2 JBI25C2B 625J2 ELB-35-600-J2-AL-STD",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25DJ600.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A0740041_25-35kV_600A_JUNCTION_Instructions.pdf",
      video           : "", 
    )); 
    //PAGINA 3 004
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15/25 kV, 600A Standoff Bushing",
      partnumber      : "25-SOB600",
      campopesquisa   : "$categoria 15/25 kV, 600A Standoff Bushing 25-SOB600",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25SOB.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A0420050_15_25kV_600-900A_STANDOFF_BUSHING_Instructions.pdf",
      video           : "", 
    )); 
    //PAGINA 3 005
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV Elbow/Load Reducing Tap Plug",
      partnumber      : "25-ETP600-15-200 25-LRTP600-15-200",
      campopesquisa   : "$categoria 15 kV Elbow/Load Reducing Tap Plug 25-ETP600-15-200 25-LRTP600-15-200 650ETP BLRTP615 615ETP ELB-15/28-200-ETP15 650LT-B LRTP615 615LRTP ELB-15/28-200-LRTP15",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25-15-ETP-LRTP.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A1620050-15-25kV_600A_ETP__Insutructions.pdf",
      video           : "", 
    )); 
    //PAGINA 3 006
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV Elbow/Load Reducing Tap Plug",
      partnumber      : "25-ETP600-25-200 25-LRTP600-25-200",
      campopesquisa   : "$categoria 25 kV Elbow/Load Reducing Tap Plug 25-ETP600-15-200 25-LRTP600-15-200 K650ETP BLRTP625 625ETP ELB-15/28-200-ETP25 K650LT-B LRTP625 625LRTP ELB-15/28-200-LRTP25",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/25-25ETP-LRTP.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A1620050-15-25kV_600A_ETP__Insutructions.pdf",
      video           : "", 
    )); 
    //PAGINA 3 007
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Multi Point Junction",
      partnumber      : "MPJ",
      campopesquisa   : "$categoria Multi Point Junction MPJ 628TM",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/MPJ.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A3246040_25kV_200600A__Multiple_Junction.pdf",
      video           : "", 
    )); 
    //PAGINA 3 008
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV, Insulating Plug",
      partnumber      : "25-IP600",
      campopesquisa   : "$categoria 25 kV, Connecting Plug Threaded Stud Compression Connector Cable Adapter Shear Bolted Connector 25-DIC200 25-IP600C 25-DCP 25-STUD 600BMC 25-ADP SBC 625BIP K650BIP DIP625A",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/1525-600A-Replacement_part.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //PAGINA 3 009
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV, Insulating Plug",
      partnumber      : "35-IP600",
      campopesquisa   : "$categoria 35 kV, Insulating Plug 35 kV, Connecting Plug Threaded Stud Compression Connector Cable Adapter Shear Bolted Connector 35-IP600 35-DCP 35-STUD 600BMC 35-ADP SBC",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35-600A-Replacement_part.pdf",
      instrucao       : "",
      video           : "", 
    )); 
    //PAGINA 3 010
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV, 600A, Deadbreak T-Body",
      partnumber      : "35-TB600",
      campopesquisa   : "$categoria 35 kV, 600A, Deadbreak T-Body 35-TB600",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35TB600.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/35-TBODY-_INSTALLATION_SHEET-170921.pdf",
      video           : "", 
    )); 
    //PAGINA 3 011
    pagina=3;
    posicao++;
    categoria = "IEEE/ANSI 600A Deadbreak";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV, 600A Deadbreak Junction",
      partnumber      : "35-DJ600",
      campopesquisa   : "$categoria 35 kV, 600A Deadbreak Junction 35-DJ600",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35DJ600.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/A0740041_25-35kV_600A_JUNCTION_Instructions.pdf",
      video           : "", 
    ));                                     
    posicao = 0;




    //PAGINA 4 001
    pagina=4;
    posicao++;
    categoria = "IEC Interface A";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "24kV 250A Insulated Protective Cap",
      partnumber      : "24-DIC250",
      campopesquisa   : "$categoria 24-DIC250 24kV 250A Insulated Protective Cap",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24DIC250.pdf",
      instrucao       : "",
      video           : "", 
    ));                                     
    //PAGINA 4 002
    pagina=4;
    posicao++;
    categoria = "IEC Interface A";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 250A Straight Connector",
      partnumber      : "24-CL250",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 250A Straight Connector 24-CL250",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24CL250.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/24CL250-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    ));                  
    //PAGINA 4 003
    pagina=4;
    posicao++;
    categoria = "IEC Interface A";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 250A Elbow Connector",
      partnumber      : "24-CE250",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 250A Elbow Connector 24-CE250",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24CE250.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/24CE250-INSTRUCTION_SHEET-20161117.pdf",
      video           : "", 
    ));                  
    //PAGINA 4 004
    pagina=4;
    posicao++;
    categoria = "IEC Interface A";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "24kV 250A Deadbreak Bushing Insert",
      partnumber      : "24-DBI250",
      campopesquisa   : "$categoria 24kV 250A Deadbreak Bushing Insert 24-DBI250",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24DBI250.pdf",
      instrucao       : "",
      video           : "", 
    ));    
    posicao=0;       


    //PAGINA 5 001
    pagina=5;
    posicao++;
    categoria = "IEC Interface B";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "36kV 400A Front T-body",
      partnumber      : "36-FDT400",
      campopesquisa   : "$categoria 36kV 400A Front T-body 36-FDT400",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/36FDT400.pdf",
      instrucao       : "",
      video           : "", 
    ));   




    posicao=0;
    //PAGINA 6 001
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17 kV / 50 kV Coupling T Surge Arrester",
      partnumber      : "17-RDTA50",
      campopesquisa   : "$categoria 17 kV / 50 kV Coupling T Surge Arrester 17-RDTA50",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/17RDTA50.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 002
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "26 kV / 66 kV Coupling T Surge Arrester	",
      partnumber      : "26-RDTA66",
      campopesquisa   : "$categoria 26 kV / 66 kV Coupling T Surge Arrester	26-RDTA66",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/26RDTA66.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 003
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 630A Front T-Body",
      partnumber      : "24-FDT630",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Front T-Body 24-FDT630",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24FDT630-RDT630.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/9-135004P010-17.5%2624kV_630A_IEC_T-body_Installation_Instructions.pdf",
      video           : "", 
    ));       
    //PAGINA 6 004
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 630A Rear T-Body",
      partnumber      : "24-RDT630",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Rear T-Body 24-RDT630",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24FDT630-RDT630.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/9-135004P010-17.5%2624kV_630A_IEC_T-body_Installation_Instructions.pdf",
      video           : "", 
    ));       
    //PAGINA 6 005
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 630A Large Front T-Body",
      partnumber      : "24-LFDT630",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Large Front T-Body 24-LFDT630",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24LFDT630-24LRDT630.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 006
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 630A Large Rear T-Body",
      partnumber      : "24-LRDT630",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Large Rear T-Body 24-LRDT630",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24LFDT630-24LRDT630.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 007
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "24kV 630A Insulated Protective Cap",
      partnumber      : "24-DIC630",
      campopesquisa   : "$categoria 24kV 630A Insulated Protective Cap 24-DIC630",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/24-DIC630.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 008
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "34 kV / 85 kV Coupling T Surge Arrester	",
      partnumber      : "34-RDTA85",
      campopesquisa   : "$categoria 34 kV / 85 kV Coupling T Surge Arrester	34-RDTA85",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/34RDTA85.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 009
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "36 kV 630A Front T-Body",
      partnumber      : "36-FDT630",
      campopesquisa   : "$categoria 36 kV 630A Front T-Body 36-FDT630",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/36FDT630-RDT630.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/36FDT630%2636RDT630-Instruction_Sheet-20180404.pdf",
      video           : "", 
    ));       
    //PAGINA 6 010
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-0$posicao",
      categoria       : "$categoria",
      descricao       : "36 kV 630A Rear T-Body",
      partnumber      : "36-RDT630",
      campopesquisa   : "$categoria 36 kV 630A Rear T-Body 36-RDT630",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/36FDT630-RDT630.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/IEC/36FDT630%2636RDT630-Instruction_Sheet-20180404.pdf",
      video           : "", 
    ));       
    //PAGINA 6 011
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-0$posicao",
      categoria       : "$categoria",
      descricao       : "42 kV 1250A Front T-Body",
      partnumber      : "42-FDT1250",
      campopesquisa   : "$categoria 42 kV 1250A Front T-Body 42-FDT1250",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/42FDT1250-RDT1250.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/42kV_1250A_FDT_RDT_installation_instruction_CHARDON.pdf",
      video           : "", 
    ));       
    //PAGINA 6 012
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-0$posicao",
      categoria       : "$categoria",
      descricao       : "42 kV 1250A Rear T-Body",
      partnumber      : "42-RDT1250",
      campopesquisa   : "$categoria 42 kV 1250A Rear T-Body 42-RDT1250",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/42FDT1250-RDT1250.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/600A/42kV_1250A_FDT_RDT_installation_instruction_CHARDON.pdf",
      video           : "", 
    ));       
    //PAGINA 6 013
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-0$posicao",
      categoria       : "$categoria",
      descricao       : "42 kV 1250A Bushing Extender",
      partnumber      : "42-BE1250",
      campopesquisa   : "$categoria 42 kV 1250A Bushing Extender 42-BE1250",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/42-BE1250.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 6 014
    pagina=6;
    posicao++;
    categoria = "IEC Interface C";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-0$posicao",
      categoria       : "$categoria",
      descricao       : "51 kV / 134 kV Coupling T Surge Arrester",
      partnumber      : "51-RDTA134",
      campopesquisa   : "$categoria 51 kV / 134 kV Coupling T Surge Arrester 51-RDTA134",
      assetImagem     : "assets/products/P00$pagina/0$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/0$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/IECCABLEACCESSORIESCTCONLY/51RDTA134.pdf",
      instrucao       : "",
      video           : "", 
    ));       




    posicao=0;
    //PAGINA 7 001
    pagina=7;
    posicao++;
    categoria = "Transformer Components";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Sidewall/Cover Mounted Fuse Holder Assembly",
      partnumber      : "CHBON Series",
      campopesquisa   : "$categoria Sidewall/Cover Mounted Fuse Holder Assembly CHBON Series",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/TRANSFORMER/CHBON.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 7 002
    pagina=7;
    posicao++;
    categoria = "Transformer Components";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Chardon 15, 25, 28 kV 200A Bushing Well",
      partnumber      : "CH200BW",
      campopesquisa   : "$categoria Chardon 15, 25, 28 kV 200A Bushing Well CH200BW",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/CH200BW.pdf",
      instrucao       : "",
      video           : "", 
    ));       
    //PAGINA 7 003
    pagina=7;
    posicao++;
    categoria = "Transformer Components";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Isolation Link",
      partnumber      : "CHIL",
      campopesquisa   : "$categoria Isolation Link CHIL",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/TRANSFORMER/CHIL.pdf",
      instrucao       : "",
      video           : "", 
    ));           


    posicao=0;
    //PAGINA 8 001
    pagina=8;
    posicao++;
    categoria = "Epoxy Products";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV 200A Lodabreak Integral Bushing",
      partnumber      : "15-LIB130 15-LIB185",
      campopesquisa   : "$categoria 15 kV 200A Lodabreak Integral Bushing 15-LIB130 15-LIB185",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/EXPOXYctconly/15LIB130-LIB185.pdf",
      instrucao       : "",
      video           : "", 
    ));   
    //PAGINA 8 002
    pagina=8;
    posicao++;
    categoria = "Epoxy Products";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "17.5 kV / 24 kV 630A Apparatus Bushing",
      partnumber      : "24-TCP630 36-TCP630 42-TCP1250",
      campopesquisa   : "$categoria 17.5 kV / 24 kV 630A Apparatus Bushing 24-TCP630 36-TCP630 42-TCP1250",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/EXPOXYctconly/24TCP.pdf",
      instrucao       : "",
      video           : "", 
    ));   
    //PAGINA 8 003
    pagina=8;
    posicao++;
    categoria = "Epoxy Products";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "25 kV Deadbreak 600A/900A/Insulated Plug",
      partnumber      : "25-IP600 25-DCP600 25-DCP900",
      campopesquisa   : "$categoria 25 kV Deadbreak Insulated Plug 25kV Deadbreak 600A Connecting Plug 25 kV Deadbreak 900A Connecting Plug 25-IP600 25-IP600 25-DCP600 25-DCP900",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/1525-600A-Replacement_part.pdf",
      instrucao       : "",
      video           : "", 
    ));   
    //PAGINA 8 004
    pagina=8;
    posicao++;
    categoria = "Epoxy Products";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35 kV Deadbreak 600A/900A/Insulated Plug",
      partnumber      : "35-IP600 35-DCP600 35-DCP900",
      campopesquisa   : "$categoria 35-IP600 35-DCP600 35-DCP900 35 kV Deadbreak Insulated Plug 35kV Deadbreak 600A Connecting Plug 35 kV Deadbreak 900A Connecting Plug",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/600ADEADBREAKCTCONLY/35-600A-Replacement_part.pdf",
      instrucao       : "",
      video           : "", 
    ));   
    
    posicao=0;
    //PAGINA 9 001
    pagina=9;
    posicao++;
    categoria = "Switchgear Products";
    itemsChardom.add(ItemChardon(
      id              : "P00$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Chardon Epoxy Solid Recloser",
      partnumber      : "CKGR",
      campopesquisa   : "$categoria Chardon Epoxy Solid Recloser CKGR",
      assetImagem     : "assets/products/P00$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P00$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/Switchgear/CKGR.pdf",
      instrucao       : "",
      video           : "", 
    ));   



    posicao=0;
    //PAGINA 10 001
    pagina=10;
    posicao++;
    categoria = "Cold Shrinkable Products";
    itemsChardom.add(ItemChardon(
      id              : "P0$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "15 kV, 25 kV and 35kV Cold Shrinkable Termination",
      partnumber      : "15/25-CSTO",
      campopesquisa   : "$categoria 15 kV, 25 kV and 35kV Cold Shrinkable Termination 15-CSTO 25-CSTO",
      assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/ColdShrinkCTConly/152535CSTO.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/ColdShrinK/15%2625kVCSTO-Installation_sheet_-20190315.PDF",
      video           : "", 
    ));   
    //PAGINA 10 002
    pagina=10;
    posicao++;
    categoria = "Cold Shrinkable Products";
    itemsChardom.add(ItemChardon(
      id              : "P0$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "35kV Cold Shrinkable Termination",
      partnumber      : "35-CSTO",
      campopesquisa   : "$categoria 35kV Cold Shrinkable Termination 35-CSTO",
      assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/ColdShrinkCTConly/152535CSTO.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/ColdShrinK/35kVCSTO-Installation_sheet_-20190315.PDF",
      video           : "", 
    ));     
    //PAGINA 10 003
    pagina=10;
    posicao++;
    categoria = "Cold Shrinkable Products";
    itemsChardom.add(ItemChardon(
      id              : "P0$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Cold Shrinkable Jacket Seal",
      partnumber      : "CJS",
      campopesquisa   : "$categoria Cold Shrinkable Jacket Seal CJS",
      assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/ColdShrinkCTConly/CJS.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/ColdShrinK/CJS-INSTRUCTION_SHEET-20181222_.pdf",
      video           : "", 
    ));       

    posicao=0;
    //PAGINA 11 001
    pagina=11;
    posicao++;
    categoria = "Other Connectors";
    itemsChardom.add(ItemChardon(
      id              : "P0$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Shear Bolt Connector",
      partnumber      : "SBC",
      campopesquisa   : "$categoria Shear Bolt Connector SBC",
      assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/OtherCTConlly/SBC.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/OTHER/Shear_Bolt_Connector-Installation_Instruction.pdf",
      video           : "", 
    ));   
    //PAGINA 11 002
    pagina=11;
    posicao++;
    categoria = "Other Connectors";
    itemsChardom.add(ItemChardon(
      id              : "P0$pagina-00$posicao",
      categoria       : "$categoria",
      descricao       : "Submersible Low Voltage Connector",
      partnumber      : "SLVC - BMI",
      campopesquisa   : "$categoria Submersible Low Voltage Connector SLVC BMI",
      assetImagem     : "assets/products/P0$pagina/00$posicao.jpg",
      assetImagemMini : "assets/products/P0$pagina/mini/00$posicao.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/OtherCTConlly/SLVC.pdf",
      instrucao       : "http://www.chardongroup.com/upload/web/InstructionSheet/OTHER/SLVC_instruction_sheet.pdf",
      video           : "", 
    ));   
    //PAGINA 11 003
    pagina=11;
    posicao++;
    categoria = "Other Connectors";
    itemsChardom.add(ItemChardon(
      id              : "P011-003",
      categoria       : "$categoria",
      descricao       : "T-Wrench 5/16-inch Hex Wrench",
      partnumber      : "CT-TW001",
      campopesquisa   : "$categoria T-Wrench 5/16-inch Hex Wrench CT-TW001",
      assetImagem     : "assets/products/P011/003.jpg",
      assetImagemMini : "assets/products/P011/mini/003.png",
      catalogo        : "http://www.chardongroup.com/upload/web/Catalogs/EnglishCatalog/OtherCTConlly/CTTW001.pdf",
      instrucao       : "",
      video           : "", 
    ));           

    return itemsChardom;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(gbl_Idiomas_indice[indice_idioma]["tituloProdutos"]),        
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return index == 0 ? _searchBar() : _listItem(index-1);
        },
        itemCount: _notesForDisplay.length+1,
      ),
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
          hintText: gbl_Idiomas_indice[indice_idioma]["procurar"],
        ),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _notesForDisplay = _notes.where((note) {
              var noteTitle = note.campopesquisa.toLowerCase();
              return noteTitle.contains(text);
            }).toList();
          });
        },
      ),
    );
  }

  _launchURL(endereco) async {
    if (await canLaunch(endereco)) {
      await launch(endereco);
    } else {
      throw 'Could not launch $endereco';
    }
  }   


  _listItem(index) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0, bottom: 32.0, left: 16.0, right: 16.0),
        child: 
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        child: Image.asset(_notesForDisplay[index].assetImagemMini,width: 80,),
                        onTap: () {
                          Navigator.push(
                            context, MaterialPageRoute(
                              builder: (context) => Imagem(_notesForDisplay[index].assetImagem, _notesForDisplay[index].descricao),
                          ));
                        },
                      ),
                      InkWell(
                        child: Image.asset("assets/images/catalog.png",width: 80,),
                        onTap: () {
                          Navigator.push(
                            context, MaterialPageRoute(
                              builder: (context) => PDF(_notesForDisplay[index].catalogo),
                          ));
                        },
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context, MaterialPageRoute(
                              builder: (context) => PDF(_notesForDisplay[index].instrucao),
                          ));
                        },
                        child: _notesForDisplay[index].instrucao!=""
                          ? Image.asset("assets/images/instruction.png",width: 80,)
                          : Image.asset("assets/images/blank.png",width: 80,)
                      ),
                      _notesForDisplay[index].video != ""
                        ?InkWell(
                        onTap: () {
                          _launchURL(_notesForDisplay[index].video);
                        },
                        child:Icon(Icons.video_library, size: 40, color: Colors.redAccent,),
                      )
                      :Text("")
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Center(
                  child: Text(
                    _notesForDisplay[index].categoria,
                    style: TextStyle(
                      fontSize: 22,
                      color: Colors.blue,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    _notesForDisplay[index].descricao,
                    style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                SizedBox(height: 5,),
                Center(
                  child: Text(
                    _notesForDisplay[index].partnumber,
                    style: TextStyle(
                      color: Colors.grey.shade600,
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
      ),
    );
  }  
}