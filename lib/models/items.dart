/*
enderecço para converter JSON em classe DART

  https://javiercbk.github.io/json_to_dart/
*/

//Minha classe de Produtos da Chardon
class ItemChardon {
  String id;
  String categoria;
  String descricao;
  String partnumber;
  String campopesquisa;
  String assetImagem;
  String assetImagemMini;
  String catalogo;
  String instrucao;
  String video;

  //Constructor
  ItemChardon ({
    this.id,
    this.categoria,
    this.descricao,
    this.partnumber,
    this.campopesquisa,
    this.assetImagem,
    this.assetImagemMini,
    this.catalogo,
    this.instrucao,
    this.video,
  });

  ItemChardon.fromJason(Map<String, dynamic> json) {
    id              = json["id"];
    categoria       = json["categoria"];
    descricao       = json["descricao"];
    partnumber      = json["partnumber"];
    campopesquisa   = json["campopesquisa"];
    assetImagem     = json["assetImagem"];
    assetImagemMini = json["assetImagemMini"];
    catalogo        = json["catalogo"];
    instrucao       = json["instrucao"];
    video           = json["video"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"]              = this.id;
    data["categoria"]       = this.categoria;
    data["descricao"]       = this.descricao;
    data["partnumber"]      = this.partnumber;
    data["campopesquisa"]   = this.campopesquisa;
    data["assetImagem"]     = this.assetImagem;
    data["assetImagemMini"] = this.assetImagemMini;
    data["catalogo"]        = this.catalogo;
    data["instrucao"]       = this.instrucao;    
    data["video"]           = this.video;    
    return data;
  }
}