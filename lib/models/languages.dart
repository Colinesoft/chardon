/*
enderecço para converter JSON em classe DART

  https://javiercbk.github.io/json_to_dart/
*/

//Minha classe de Produtos da Chardon
class Idioma {
  String id;
  String idioma;
  String imagem;
  String selecionado;

  //Constructor
  Idioma ({
    this.id,
    this.idioma,
    this.imagem,
    this.selecionado,
  });

  Idioma.fromJason(Map<String, dynamic> json) {
    id                = json["id"];
    idioma            = json["idioma"];
    imagem            = json["imagem"];
    selecionado       = json["selecionado"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"]           = this.id;
    data["idioma"]       = this.idioma;
    data["imagem"]       = this.imagem;
    data["selecionado"]  = this.selecionado;
    return data;
  }
}