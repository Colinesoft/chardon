import 'dart:io';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'starter.dart';
import 'views/listaprodutos.dart';
import 'globais.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';


//import 'package:video_player/video_player.dart';
//import 'package:youtube_player/youtube_player.dart';

void main() => runApp(MyApp2()); //CHAMADA PRINCIPAL DO APP

//PÁGINA DE VÍDEOS DISPONÍVEIS CHARDON
class ListaVideos extends StatefulWidget {
  @override
  _ListaVideosState createState() => _ListaVideosState();
}
class _ListaVideosState extends State<ListaVideos> {

  Widget _listVideos(BuildContext context, String groupName, String subGroupName, String otherData, String index){
    return Card( 
      child: Padding(
        padding: EdgeInsets.only(left: 0, right: 0, top: 20),
        child: InkWell(  
          onTap: () {
            if(index=='1') _launchURL_Institucional();
            else if(index == '2') _launchURL_InstalacaoCodoOcc();
            else if(index == '3') _launchURL_InstalacaoCorpoEmT();
            else if(index == '4') _launchURL_Instalacao600A();
            else if(index == '5') _launchURL_Instalacao200A();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.play_arrow),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:<Widget>[
                        Text(
                          groupName, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(
                          subGroupName, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                          ),
                        ),
                        Text(
                          otherData, 
                          style: TextStyle(
                            fontFamily: "Arial",
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  _launchURL_Institucional() async {
    const url = 'https://www.youtube.com/watch?v=cfVtJkZXQkA&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }  
  _launchURL_InstalacaoCodoOcc() async {
    const url = 'https://www.youtube.com/watch?v=X-3_ksuNL_o&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }  
  _launchURL_InstalacaoCorpoEmT() async {
    const url = 'https://www.youtube.com/watch?v=x1BGVFD4kiQ&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }    
  _launchURL_Instalacao600A() async {
    const url = 'https://www.youtube.com/watch?v=r1upfG1c8OI&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchURL_Instalacao200A() async {
    const url = 'https://www.youtube.com/watch?v=xC5ysQid1Kc&list=PLqs8XiO86HRGomJkbBKq5Y1gyhX9Nv5Wn';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chardon Medias'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _listVideos(context, '(4:08) Institutional Video', 'PT 2018 Chardon', 'Portuguese','1'),
            _listVideos(context, '(5:55) Instalation Codo OCC', '15/25kV 200A Chardon', 'Spanish','2'),
            _listVideos(context, '(4:53) Instalation T Body', '15/25kV 600A Chardon', 'Portuguese','3'),
            _listVideos(context, '(6:48) Instalation and Operation', '15/25kV 600A Chardon', 'Portugueses','4'),
            _listVideos(context, '(7:11) TDC Instalation/Operation', '15/25kV 600A Chardon','Portuguese','5'),
          ],
        ),
      ),
    );
  }
}
// FIM PAGINA DE VIDEOS CHARDON

//PAGINA
class MenuProdutos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Segunda Rota (PAGE1)"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            //Navigator.pop(context); //OPÇÃO DE VOLTAR
            Navigator.pushNamed(context, '/');
          },
          child: Text('Retornar !'),
        ),
      ),
    );
  }
}
// FIM PAGINA

//PAGINA
class SelecionaIdioma extends StatefulWidget {
  @override
  _SelecionaIdiomaState createState() => _SelecionaIdiomaState();
}
class _SelecionaIdiomaState extends State<SelecionaIdioma> {

  int meuIndice;

  @override
  void initState() {
    _executar();
    super.initState();
  }  

  void _executar() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE cfg (idioma integer)");
      await db.execute("CREATE TABLE profile (nome TEXT, email TEXT, empresa TEXT, senha TEXT)");
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    //await database.rawInsert(
    //  'INSERT INTO cfg (idioma) values (?)', [1]
    //);        

    //SELECT
    var lista = await database.query("cfg", 
      columns: ["idioma"],
      where: "idioma>=?",
      whereArgs: ["0"]
    );
    /*print("LISTA:");
    print(lista);
    if(lista.length == 0){
      print("igual");
    } else {
      print("diferente");
    }*/
    if(lista.length==0){
      await database.rawInsert(
        'INSERT INTO cfg (idioma) values (?)', [1]
      );  
      await database.rawInsert(
        'INSERT INTO profile (nome, email, empresa, senha) values ( ?, ?, ?, ?)', 
        ["", "", "", ""]
      );      
      setState(() {
        indice_idioma = 1;
        meuIndice = 1;
      });            
    }    
    for(var item in lista){
      setState(() {
        indice_idioma = item["idioma"];
        meuIndice = item["idioma"];
      });
    }

  }
  void _trocarIdioma() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
    });

    await database.rawUpdate(
      'UPDATE cfg SET idioma=?',
      [this.meuIndice]
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chardon Preferences"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 0;
                setState(() {
                  _trocarIdioma();
                });
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/01.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gbl_Idiomas[0], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 0?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 1;
                setState(() {
                  _trocarIdioma();
                });
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/02.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gbl_Idiomas[1], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 1?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),
            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 2;
                setState(() {
                  _trocarIdioma();
                });
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/03.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gbl_Idiomas[2], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 2?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),

            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                this.meuIndice = 3;
                setState(() {
                  _trocarIdioma();
                });
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/languages/04.png", width: 80,),
                      SizedBox(width: 10,),
                      Text(gbl_Idiomas[3], style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  this.meuIndice == 3?Icon(Icons.check_box):Icon(Icons.check_box_outline_blank)
                ],
              ),

            ),
          ),
        ],
      ),
    );
  }
}
// FIM PAGINA

//PAGINA PDF
class PDF extends StatefulWidget {
  @override
  _PDFState createState() => _PDFState();
}
class _PDFState extends State<PDF> {
  bool _isLoading=false, _isInit=false;
  PDFDocument document;

    @override
  void initState() {
    loadFromAssets();
    super.initState();
  }  
  

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Column(
          children: <Widget>[
            Expanded(
              child: Center(
                child: _isInit 
                  ? Text('Press to Load...')
                  : _isLoading
                    ? Center(
                      child: CircularProgressIndicator(),
                    )
                    : PDFViewer(
                      document: document,
                    )
              ),
            )
          ],
        ),
      ),
    );
  }

  loadFromAssets() async {
    setState((){
      _isInit = false;
      _isLoading = true;
    });
    document = await PDFDocument.fromAsset("assets/ChardonCrossRef.pdf");
    setState(() {
      _isLoading = false;
    });
    
  }
}
// FIM PAGINA

//PAGINA PDF
class Perfil extends StatefulWidget {
  @override
  _PerfilState createState() => _PerfilState();
}
class _PerfilState extends State<Perfil> {

TextEditingController controller_nome = TextEditingController();
TextEditingController controller_email = TextEditingController();
TextEditingController controller_empresa = TextEditingController();
TextEditingController controller_senha = TextEditingController();
TextEditingController controller_confirmacao = TextEditingController();



  String _nome, _email, _empresa, _senha, _confirmacao;
  final formKey = GlobalKey<FormState>();


   @override
  void initState() {
    loadFromDB();
    super.initState();
  }  
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text(gbl_Idiomas_indice[indice_idioma]["perfil"]),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.check_circle),
              onPressed: (){
                saveToDB(_nome, _email, _empresa, _senha, _confirmacao);
                Navigator.pop(context);
              },
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: Padding( 
          padding: EdgeInsets.all(10), 
          child: Form(
            key: formKey,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TextField(
                    autofocus: true,
                    controller: controller_nome,
                    maxLength: 50,
                    textCapitalization: TextCapitalization.characters,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gbl_Idiomas_indice[indice_idioma]["perfil_nome"]),
                    onChanged: (value) {
                      _nome = value;
                    },
                  ),
                  TextField(
                    textCapitalization: TextCapitalization.none,
                    controller: controller_email,
                    maxLength: 50,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gbl_Idiomas_indice[indice_idioma]["perfil_email"]),
                    onChanged: (value) {
                      _email = value;
                    },
                  ),
                  TextField(
                    textCapitalization: TextCapitalization.characters,
                    controller: controller_empresa,
                    maxLength: 50,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gbl_Idiomas_indice[indice_idioma]["perfil_empresa"]),
                    onChanged: (value) {
                      _empresa = value;
                    },
                  ),
                  TextField(
                    obscureText: true,
                    controller: controller_senha,
                    maxLength: 15,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gbl_Idiomas_indice[indice_idioma]["perfil_senha"]),
                    onChanged: (value) {
                      _senha = value;
                    },
                  ),
                  TextField(
                    obscureText: true,
                    controller: controller_confirmacao,
                    maxLength: 15,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(labelText: gbl_Idiomas_indice[indice_idioma]["perfil_confirmacao"]),
                    onChanged: (value) {
                      _confirmacao = value;
                    },
                  ),
                  Icon(Icons.person_pin, size: 150, color: Colors.blue,),
                ],
              ),
          ),
          ),
        ),
      );
  }

  Future saveToDB(nome, email, empresa, senha, confirmacao) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { 
    },
    onCreate: (Database db, int version) async {
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    if(senha == confirmacao && senha != null) {
      print(nome);
      print(email);
      print(empresa);
      print(senha);
      await database.rawInsert(
        'UPDATE profile SET nome=?, email=?, empresa=?, senha=?', 
        [nome, email, empresa, senha]
      );        
    } else {
      return false;
    }
  }

  loadFromDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    //await database.rawInsert(
    //  'INSERT INTO cfg (idioma) values (?)', [1]
    //);        

    //SELECT
    var lista = await database.query("profile", 
      columns: ["nome", "email", "empresa", "senha"],
    );    
    if(lista.length == 0){
      setState(() {
        _nome = "";
        _email = "";
        _empresa = "";
        _senha = "";
        _confirmacao = ""; 

        controller_nome.text = _nome;
        controller_email.text = _email;
        controller_empresa.text = _empresa;
        controller_senha.text = _senha;
        controller_confirmacao.text = _confirmacao;
      });      
    } else {    
      for(var item in lista){
        setState(() {
          _nome = item["nome"];
          _email = item["email"];
          _empresa = item["empresa"];
          _senha = item["senha"];
          _confirmacao = item["senha"]; 

          controller_nome.text = _nome;
          controller_email.text = _email;
          controller_empresa.text = _empresa;
          controller_senha.text = _senha;
          controller_confirmacao.text = _confirmacao;
        });
      }
    }
  }
}
// FIM PAGINA

// CLASSE PRINCIPAL DO APP
class MyApp2 extends StatefulWidget {
  @override
  _MyApp2State createState() => _MyApp2State();
}
class _MyApp2State extends State<MyApp2> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //DESATIVA O BANNER DE DEBUG
      title: 'Chardon Group Catalog',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: <String, WidgetBuilder>{
        "/": (BuildContext context) => new MyPage(title: 'Chardon Group'),
        "/menuprodutos": (BuildContext context) => new MenuProdutos(),
        "/listavideos": (BuildContext context) => new ListaVideos(),
        "/listaprodutos": (BuildContext context) => new ListaProdutos(),
        "/idioma": (BuildContext context) => new SelecionaIdioma(),
        "/PDF" : (BuildContext context) => new PDF(),
        "/perfil" : (BuildContext context) => new Perfil(),
        "/login" : (BuildContext context) => new Starter(),
        
      },
      initialRoute: "/",
      //home: MyHomePage(title: 'Chardon Group'), //Normalmente a chamada a pagina principal é feita assim
    );
  }
}

//CustomListTile -> Personalizando o item do meu Drawer
class CustomListTile extends StatelessWidget{
  //construtor para poder passar parâmetros ao widget
  IconData icon;
  String description;
  Function onTap;

  CustomListTile(this.icon, this.description, this.onTap);
  
  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[300])),
        ),
        child: InkWell(
          splashColor: Colors.blueAccent,
          onTap: onTap,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(icon),
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Text(description, style: TextStyle(fontSize: 16, ),),
                    ),
                  ],
                ),
                Icon(Icons.arrow_right),
              ],
            ),
          ),
        )
      ),
    );
  }
}

class MyPage extends StatefulWidget {
  MyPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyPageState createState() => _MyPageState();
}
class _MyPageState extends State<MyPage> {

  int _counter = 0;

  int meuIndice;

  @override
  void initState() {
    _executar();
    super.initState();
  }  

  void _executar() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE cfg (idioma integer)");
      await db.execute("CREATE TABLE profile (nome TEXT, email TEXT, empresa TEXT, senha TEXT)");
    });

    //DELETE
    //await database.rawDelete(
    //  'DELETE FROM cfg',
    //);

    //INSERT
    //await database.rawInsert(
    //  'INSERT INTO cfg (idioma) values (?)', [1]
    //);        

    //SELECT
    var lista = await database.query("cfg", 
      columns: ["idioma"],
      where: "idioma>=?",
      whereArgs: ["0"]
    );    
    if(lista.length == 0){
      await database.rawInsert(
        'INSERT INTO cfg (idioma) values (?)', [1]
      );
      await database.rawInsert(
        'INSERT INTO profile (nome, email, empresa, senha) values ( ?, ?, ?, ?)', 
        ["", "", "", ""]
      );  
      setState(() {
        indice_idioma = 1;
        meuIndice = 1;
      });      
    }    
    for(var item in lista){
      setState(() {
        indice_idioma = item["idioma"];
        meuIndice = item["idioma"];
      });
    }

  }
  void _trocarIdioma() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/chardon.db";

    var database = await openDatabase(path, version: 1, 
    onUpgrade: (Database db, int version, int info) async { },
    onCreate: (Database db, int version) async {
    });

    await database.rawUpdate(
      'UPDATE cfg SET idioma=?',
      [this.meuIndice]
    );
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  } //ESPECÍFICO PARA CONTADOR DA PÁGINA INICIAL
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: new Image.asset(
                'assets/images/logo.png',
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
            CustomListTile(Icons.person, gbl_Idiomas_indice[indice_idioma]["perfil"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/perfil"); }
            ),
            CustomListTile(Icons.apps, gbl_Idiomas_indice[indice_idioma]["catalogo"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/login"); }
            ),
            CustomListTile(Icons.video_library, gbl_Idiomas_indice[indice_idioma]["youtube"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/listavideos"); }
            ),
            CustomListTile(Icons.file_download, gbl_Idiomas_indice[indice_idioma]["cross"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/PDF"); }
            ),
            CustomListTile(Icons.tune, gbl_Idiomas_indice[indice_idioma]["idioma"], (){
              Navigator.pop(context);
              Navigator.pushNamed(context, "/idioma"); }
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/images/back4.jpg',
          ),
          SizedBox(height: 40),
          
          Image.asset(
            'assets/images/back11.jpg',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //BOTÃO DA PÁGINA INICIAL QUE VOU MANDAR PARA O VÍDEO DE APRESENTAÇÃO
          //Navigator.pushNamed(context, '/listaprodutos');
          _incrementCounter();
          Navigator.pushNamed(context, '/login');
          //_launchURL();
        },
        tooltip: 'products',
        child: Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}





class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Access chardon',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
